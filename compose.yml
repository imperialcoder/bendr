---
services:
  glance:
    build: docker/glance
    network_mode: host
    container_name: glance
    restart: unless-stopped
    volumes:
      - ${CONFIG_DIR}/glance/glance.yml:/app/glance.yml
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro

  jellyfin:
    build: docker/jellyfin
    container_name: jellyfin
    privileged: true
    network_mode: host
    environment:
      NVIDIA_DRIVER_CAPABILITIES: all
      NVIDIA_VISIBLE_DEVICES: all
      TZ: America/Los_Angeles
    volumes:
      - ${CONFIG_DIR}/jellyfin/config:/config
      - ${CONFIG_DIR}/jellyfin/cache:/cache
      - ${MEDIA_DIR}:/media
    restart: unless-stopped
    # for amd gpu
    devices:
      - /dev/dri/renderD128:/dev/dri/renderD128
    # for nvidia gpu
    # deploy:
    #  resources:
    #    reservations:
    #      devices:
    #          - capabilities: [gpu]

  # plex:
  #  build: docker/plex
  #  container_name: plex
  #  network_mode: host
  #  environment:
  #    - PUID=1000
  #    - PGID=1000
  #    - VERSION=docker
  #  # for nvidia gpu
  #  #- NVIDIA_DRIVER_CAPABILITIES=all
  #  #- NVIDIA_VISIBLE_DEVICES=all
  #  volumes:
  #    - ${CONFIG_DIR}/plex:/config
  #    - ${MEDIA_DIR}:/media
  #  restart: unless-stopped

  navidrome:
    build: docker/navidrome
    container_name: navidrome
    environment:
      ND_SCANSCHEDULE: 1h
      ND_LOGLEVEL: info
      ND_SESSIONTIMEOUT: 24h
      ND_BASEURL: ''
    ports: [4533:4533]
    volumes: ['${CONFIG_DIR}/navidrome/:/data', '${MEDIA_DIR}/music:/music:ro']
    restart: unless-stopped

  jellyseer:
    build: docker/jellyseer
    container_name: jellyseer
    environment: [LOG_LEVEL=debug, TZ=America/Denver]
    ports: [5055:5055]
    volumes: ['${CONFIG_DIR}/jellyseer:/app/config']
    restart: unless-stopped

  # arm:
  #  build: docker/arm
  #  container_name: arm
  #  privileged: true
  #  deploy:
  #    resources:
  #      limits:
  #        cpus: '16'
  #  logging:
  #    options:
  #      max-size: 10m
  #  volumes:
  #    - ${CONFIG_DIR}/arm/:/home/arm/
  #    - ${MEDIA_DIR}/rips:/home/arm/media
  #  restart: unless-stopped
  #  devices: [/dev/sr0:/dev/sr0]
  #  ports:
  #    - 8181:8080

  wireguard:
    build: docker/wireguard
    container_name: wireguard
    restart: unless-stopped
    cap_add: [NET_ADMIN, SYS_MODULE]
    environment: [PUID=1000, PGID=1000, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/wireguard:/config', /lib/modules:/lib/modules]
    ports: [8080:8080, 51820:51820/udp]
    sysctls: [net.ipv4.conf.all.src_valid_mark=1]

  sabnzbd:
    build: docker/sabnzbd
    container_name: sabnzbd
    restart: unless-stopped
    environment: [PUID=1000, PGID=1000, TZ=Etc/UTC]
    volumes:
      - ${CONFIG_DIR}/sabnzbd:/config
      - ${MEDIA_DIR}/usenet/complete:/downloads
      - ${MEDIA_DIR}/usenet/incomplete:/incomplete-downloads
    ports: [8081:8080]

  qbittorrent:
    build: docker/qbittorrent
    container_name: qbittorrent
    network_mode: service:wireguard
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver, WEBUI_PORT=8080]
    volumes:
      - ${CONFIG_DIR}/qbittorrent:/config
      - ${MEDIA_DIR}/torrents:/downloads
      - ${MEDIA_DIR}:/media

  prowlarr:
    build: docker/prowlarr
    container_name: prowlarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/prowlarr:/config', '${MEDIA_DIR}:/media']
    ports: [9696:9696]

  lidarr:
    build: docker/lidarr
    container_name: lidarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/lidarr:/config', '${MEDIA_DIR}:/media']
    ports: [8686:8686]

  readarr:
    build: docker/readarr
    container_name: readarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/readarr:/config', '${MEDIA_DIR}:/media']
    ports: [8787:8787]

  radarr:
    build: docker/radarr
    container_name: radarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/radarr:/config', '${MEDIA_DIR}:/media']
    ports: [7878:7878]

  sonarr:
    build: docker/sonarr
    container_name: sonarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/sonarr:/config', '${MEDIA_DIR}:/media']
    ports: [8989:8989]

  whisparr:
    container_name: whisparr
    build: docker/whisparr
    environment: [PUID=0, PGID=0, TZ=American/Denver]
    volumes: ['${CONFIG_DIR}/whisparr:/config', '${MEDIA_DIR}:/media']
    ports: [6969:6969]

  bazarr:
    build: docker/bazarr
    container_name: bazarr
    restart: unless-stopped
    environment: [PUID=0, PGID=0, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/bazarr:/config', '${MEDIA_DIR}:/media']
    ports: [6767:6767]

  tdarr:
    build: docker/tdarr
    container_name: tdarr
    restart: unless-stopped
    network_mode: bridge
    ports:
      - 8265:8265  # webUI port
      - 8266:8266  # server port
      - 8267:8267  # Internal node port
      - 8268:8268  # Example extra node port
    environment:
      - TZ=America/Denver
      - UMASK_SET=002
      - serverIP=0.0.0.0
      - serverPort=8266
      - webUIPort=8265
      - internalNode=true
      - inContainer=true
      - nodeName=MyInternalNode
    volumes:
      - ${CONFIG_DIR}/tdarr/server:/app/server
      - ${CONFIG_DIR}/tdarr/configs:/app/configs
      - ${CONFIG_DIR}/tdarr/logs:/app/logs
      - ${MEDIA_DIR}:/media
      - ${CONFIG_DIR}/tdarr/cache:/temp

  tdarr-node:
    build: docker/tdarr_node
    container_name: tdarr-node
    restart: unless-stopped
    network_mode: service:tdarr
    devices: [/dev/dri/renderD128:/dev/dri/renderD128]
    environment:
      - TZ=America/Denver
      - UMASK_SET=002
      - nodeName=MainNode
      - serverIP=0.0.0.0
      - serverPort=8266
      - inContainer=true
    volumes:
      - ${CONFIG_DIR}/tdarr/configs:/app/configs
      - ${CONFIG_DIR}/tdarr/logs:/app/logs
      - ${MEDIA_DIR}:/media
      - ${CONFIG_DIR}/tdarr/cache:/temp

  freshrss:
    build: docker/freshrss
    container_name: freshrss
    environment: [PUID=1000, PGID=1000, TZ=America/Denver]
    volumes: ['${CONFIG_DIR}/freshrss:/config']
    ports: [8888:80]
    restart: unless-stopped

# we will keep trying on this guy. lots of potential
# rssbox:
#  build: docker/rssbox
#  container_name: rssbox
#  restart: unless-stopped
#  entrypoint: tail -f /dev/null
