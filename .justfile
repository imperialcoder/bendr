# list all just commands
default:
    @just --list --unsorted

# will update the docker images
update:
    docker compose build --pull
    docker compose up -d
